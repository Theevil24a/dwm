#include "fibonacci.c"
#include <X11/XF86keysym.h>
/*
      ██
     ░██
     ░██ ███     ██ ██████████
  ██████░░██  █ ░██░░██░░██░░██
 ██░░░██ ░██ ███░██ ░██ ░██ ░██
░██  ░██ ░████░████ ░██ ░██ ░██
░░██████ ███░ ░░░██ ███ ░██ ░██
 ░░░░░░ ░░░    ░░░ ░░░  ░░  ░░

 ａｕｔｈｏｒ  ＠ｔｈｅｅｖｉｌ２４ａ
*/

/* appearance */
static const unsigned int borderpx = 1;
static const unsigned int snap = 1;
static const int showbar = 1;
static const int topbar = 1;
static const char *fonts[] = {"Terminus:size=9"};
static const char dmenufont[] = "Terminus:size=9";
static const char col_gray1[] = "#222222";
static const char col_gray2[] = "#3b3750";
static const char col_gray3[] = "#bbbbbb";
static const char col_gray4[] = "#eeeeee";
static const char col_cyan[] = "#161526";
static const char *colors[][3] = {
    [SchemeNorm] = {col_gray3, col_gray1, col_gray2},
    [SchemeSel] = {col_gray4, col_cyan, col_cyan},
};

/* tagging */
static const char *tags[] = {"一", "二", "三", "九", "八", "七", "六", "五", "四", "十", "十一", "十二", "十三", "十四", "十五"};

/* custom symbols for nr. of clients in monocle layout */
/* when clients >= LENGTH(monocles), uses the last element */
static const char *monocles[] = {"[一]", "[二]", "[三]", "[九]", "[八]", "[七]", "[六]", "[五]", "[四]"};

static const Rule rules[] = {
    {"Gimp", NULL, NULL, 0, 1, -1},
};

/* layout(s) */
static const float mfact = 0.60;
static const int nmaster = 1;
static const int resizehints = 0;
static const int lockfullscreen = 1;
static const unsigned int gappx = 0.8;

static const Layout layouts[] = {
    {"[|]", dwindle},
    {"[M]", monocle},
    {"[]=", tile},
    {"(@)", spiral},
    {"TTT", bstack},
    {"===", bstackhoriz},
    {"|M|", centeredmaster},
    {">M>", centeredfloatingmaster},
    {"[D]", deck},
    {"><>", NULL},
};

/* key definitions */
#define MODKEY Mod1Mask
#define MODKEYSECOND Mod4Mask
#define TAGKEYS(KEY, TAG)\
    {MODKEY, KEY, view, {.ui = 1 << TAG}},\
    {MODKEY| ControlMask, KEY, toggleview, {.ui = 1 << TAG}},\
    {MODKEY| ShiftMask, KEY, tag, {.ui = 1 << TAG}}, \
    {MODKEY| ControlMask | ShiftMask, KEY, toggletag, {.ui = 1 << TAG}},

#define SHCMD(cmd)\
    {\
        .v = (const char *[]) { "/bin/sh", "-c", cmd, NULL }\
    }

/* commands */
static const char *dmenucmd[] = {"dmenu_run", "-p", "==: ",  "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL};
static const char *rofimenu[] = {"rofi", "-show", "drun", NULL};
static const char *termcmd[] = {"alacritty", NULL};
static const char *upvol[] = {"/usr/bin/pactl", "set-sink-volume", "0", "+5%", NULL};
static const char *downvol[] = {"/usr/bin/pactl", "set-sink-volume", "0", "-5%", NULL};
static const char *mutevol[] = {"/usr/bin/pactl", "set-sink-mute", "0", "toggle", NULL};
static char dmenumon[2] = "0";

static Key keys[] = {
    //APPS
    {MODKEY, XK_d, spawn, {.v = dmenucmd}},                                // run dmwnu
    {MODKEY, XK_p, spawn, {.v = rofimenu}},                                // run rofi menu 
    {MODKEY, XK_Return, spawn, {.v =termcmd}},                             // run terminal 
    {MODKEY | ShiftMask, XK_y, spawn, SHCMD("google-chrome-stable")},      // run browser
    {MODKEY, XK_e, spawn, SHCMD("thunar")},                                // run thunar
    {MODKEY, XK_x, spawn, SHCMD("bash ~/.config/dwm/scripts/power")},      // menu logout 
    {MODKEY | ShiftMask, XK_l, spawn, SHCMD("bash ~/my-i3lock-config.sh")},// locksreeen
    {MODKEY, XK_s, spawn, SHCMD("flameshot gui")},                         //  -------- 
    //TAGS
    {MODKEY | ShiftMask, XK_m, setlayout, {.v = &layouts[0]}},             // monocle
    {MODKEY | ShiftMask, XK_n, setlayout, {.v = &layouts[1]}},             // tile
    {MODKEY | ShiftMask, XK_e, setlayout, {.v = &layouts[2]}},             // dwindle
    {MODKEY | ShiftMask, XK_r, setlayout, {.v = &layouts[3]}},             // spiral
    {MODKEY | ShiftMask, XK_t, setlayout, {.v = &layouts[4]}},             // bstack
    {MODKEY | ShiftMask, XK_a, setlayout, {.v = &layouts[5]}},             // bstackhoriz
    {MODKEY | ShiftMask, XK_s, setlayout, {.v = &layouts[6]}},             // centeredmaster
    {MODKEY | ShiftMask, XK_d, setlayout, {.v = &layouts[7]}},             // centeredfloatingmaster
    {MODKEY | ShiftMask, XK_f, setlayout, {.v = &layouts[9]}},             // float
    {MODKEY | ShiftMask, XK_g, setlayout, {.v = &layouts[8]}},             // deck
    //CIRCLE LAYOUT                                                                       
    {MODKEY | ControlMask, XK_comma, cyclelayout, {.i = -1}},
    {MODKEY | ControlMask, XK_period, cyclelayout, {.i = +1}},
    //RESIZE                                                                           
    {MODKEY | ShiftMask, XK_j, aspectresize, {.i = +24}},
    {MODKEY | ShiftMask, XK_k, aspectresize, {.i = -24}},
    {MODKEY | ControlMask, XK_i, incnmaster, {.i = +1}},
    {MODKEY | ControlMask, XK_o, incnmaster, {.i = -1}},
    //TOOGLE BAR
    {MODKEY | ShiftMask, XK_0, togglebar, {0}},
    //FOCUS STACK
    {MODKEY, XK_l, focusstack, {.i = +1}},
    {MODKEY, XK_h, focusstack, {.i = -1}},
    //FOCUS MONITOR
    {MODKEY, XK_comma, focusmon, {.i = -1}},
    {MODKEY, XK_period, focusmon, {.i = +1}},
    //MOVE WINDOWS BETWEEN MONITORS
    {MODKEY | ShiftMask, XK_comma, tagmon, {.i = -1}},
    {MODKEY | ShiftMask, XK_period, tagmon, {.i = +1}},
    //MOD SIZE STACK
    {MODKEY | ControlMask, XK_h, setmfact, {.f = +0.25}},
    {MODKEY | ControlMask, XK_i, setmfact, {.f = -0.25}},
    {MODKEY | ControlMask, XK_o, setmfact, {.f = 0.00}},
    //DWM FUNCTIONS
    {MODKEY, XK_v, zoom, {0}},
    {MODKEY, XK_Tab, view, {0}},
    {MODKEY | ShiftMask, XK_c, killclient, {0}},
    {MODKEY | ShiftMask, XK_space, togglefloating, {0}},
    {MODKEY | ControlMask, XK_q, quit, {0}},
    {MODKEY, XK_g, spawn, {.v = mutevol}},
    {MODKEY, XK_y, spawn, {.v = downvol}},
    {MODKEY, XK_u, spawn, {.v = upvol}},
    {0, XF86XK_AudioRaiseVolume, spawn, {.v = upvol}},
    {0, XF86XK_AudioLowerVolume, spawn, {.v = downvol}},
    {0, XF86XK_AudioMute, spawn, {.v = mutevol}},
  
    TAGKEYS(XK_1, 0) TAGKEYS(XK_2, 1) TAGKEYS(XK_3, 2) TAGKEYS(XK_4, 3)
    TAGKEYS(XK_5, 4) TAGKEYS(XK_6, 5) TAGKEYS(XK_7, 6) TAGKEYS(XK_8, 7)
    TAGKEYS(XK_9, 8)};

/* button definitions */
static Button buttons[] = {
    {ClkLtSymbol, 0, Button1, setlayout, {0}},
    {ClkLtSymbol, 0, Button3, setlayout, {.v = &layouts[2]}},
    {ClkWinTitle, 0, Button2, zoom, {0}},
    {ClkStatusText, 0, Button2, spawn, {.v = termcmd}},
    {ClkClientWin, MODKEY, Button1, movemouse, {0}},
    {ClkClientWin, MODKEY, Button2, togglefloating, {0}},
    {ClkClientWin, MODKEY, Button3, resizemouse, {0}},
    {ClkTagBar, 0, Button1, view, {0}},
    {ClkTagBar, 0, Button3, toggleview, {0}},
    {ClkTagBar, MODKEY, Button1, tag, {0}},
    {ClkTagBar, MODKEY, Button3, toggletag, {0}},
};
