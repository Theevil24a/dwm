#!/bin/bash
echo "INSTALLING REQUIREMNTS"
yay -S dwm dmenu rofi st alacritty nitrogen picom ttf-sazanami wqy-microhei ttf-iosevka-nerd lxappearance flameshot 
echo "COPING CONFIGURATIONS"
mkdir ~.dwm
cp configs/autostart.sh ~/.dwm/autostart.sh
cp configs/.Xresources ~/.Xresources
cp -r dwmblocks ~/.config/
cp -r dmenu ~/.config/
cp -r configs/* ~/.config/
xrdb ~/.Xresources
echo "COMPILING COMPLEMENTS"
cd ~/.config/dwmblocks
sudo make clean install
cd ~/.config/dmenu/
sudo make clean install
cd ~/.config/dwm/slstatus
sudo make clean install
echo "COMPILING CUSTOM DWM"
cd ~/.config/dwm/
sudo make clean install
echo "COMPILING CUSTOM ST"
cd st
sudo make clean install
