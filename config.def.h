#include <X11/XF86keysym.h>
#include "fibonacci.c"

/* appearance */
static const unsigned int borderpx  = 1;
static const unsigned int snap = 1;
static const int showbar = 1;
static const int topbar = 1;
static const char *fonts[] = { "Terminus:size=9" };
static const char dmenufont[] = "Terminus:size=9";
static const char col_gray1[] = "#222222";
static const char col_gray2[] = "#444444";
static const char col_gray3[] = "#bbbbbb";
static const char col_gray4[] = "#eeeeee";
static const char col_cyan[] = "#240841";
static const char *colors[][3] = {
	[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray4, col_cyan,  col_cyan  },
};

/* tagging */
static const char *tags[] = { "四", "五", "六", "七", "八", "九", "三", "二", "一" };

static const Rule rules[] = {{"Gimp", NULL, NULL, 0, 1, -1 },};

/* layout(s) */
static const float mfact = 0.55;
static const int nmaster = 1;
static const int resizehints = 0;
static const int lockfullscreen = 1;
static const unsigned int gappx = 1;

static const Layout layouts[] = {
	{ "[]=", tile },
	{ "[|]", dwindle },
	{ "[M]", monocle },
	{ "(@)", spiral },
	{ "TTT", bstack },
	{ "===", bstackhoriz },
	{ "|M|", centeredmaster },
	{ ">M>", centeredfloatingmaster },
	{ "><>", NULL },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY, KEY, view, {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask, KEY, toggleview, {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask, KEY, tag, {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY, toggletag, {.ui = 1 << TAG} },

#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static const char *dmenucmd[] = { "dmenu_run", "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL };
static const char *rofimenu[] = { "rofi", "-show", "drun", NULL };
static const char *emacs[] =    { "emacs", NULL };
static const char *termcmd[]  = { "alacritty", NULL };
static const char *upvol[]   =  { "/usr/bin/pactl", "set-sink-volume", "0", "+5%",     NULL };
static const char *downvol[] =  { "/usr/bin/pactl", "set-sink-volume", "0", "-5%",     NULL };
static const char *mutevol[] =  { "/usr/bin/pactl", "set-sink-mute",   "0", "toggle",  NULL };
static char dmenumon[2] = "0";

static Key keys[] = {
	{ MODKEY, 			  XK_d, 	   spawn, {.v = dmenucmd } },
	{ MODKEY, 			  XK_p,  	   spawn, {.v = rofimenu } },
	{ MODKEY, 			  XK_u, 	   spawn, {.v = emacs } },
	{ MODKEY, 			  XK_Return,   spawn, {.v = termcmd } },
	{ MODKEY, 			  XK_t, 	   setlayout, {.v = &layouts[0]} },// tile
	{ MODKEY, 			  XK_w, 	   setlayout, {.v = &layouts[1]} },// dwindle
	{ MODKEY, 			  XK_m, 	   setlayout, {.v = &layouts[2]} },// monocle
	{ MODKEY, 			  XK_r, 	   setlayout, {.v = &layouts[3]} },// spiral
	{ MODKEY, 			  XK_e, 	   setlayout, {.v = &layouts[4]} },// bstack
	{ MODKEY, 			  XK_n, 	   setlayout, {.v = &layouts[5]} },// bstackhoriz
	{ MODKEY, 			  XK_g, 	   setlayout, {.v = &layouts[6]} },// centeredmaster
	{ MODKEY, 			  XK_q, 	   setlayout, {.v = &layouts[7]} },// centeredfloatingmaster
	{ MODKEY, 			  XK_f, 	   setlayout, {.v = &layouts[8]} },// NULL
	{ MODKEY, 			  XK_s, 	   spawn, SHCMD("/bin/scrot -q 100 -e 'mv $f ~/Pictures/screenshot'") },
	{ MODKEY|ShiftMask,   XK_s,        spawn, SHCMD("/bin/scrot -s -q 100 -e 'mv $f ~/Pictures/screenshot'") },
	{ MODKEY|ShiftMask,   XK_b,        spawn, SHCMD("brave") },
	{ MODKEY|ShiftMask,   XK_e,        spawn, SHCMD("pcmanfm") },
	{ MODKEY, 			  XK_x, 	   spawn, SHCMD("bash ~/.config/dwm/scripts/power") },
	{ MODKEY, 			  XK_b, 	   togglebar, {0} },
	{ MODKEY, 			  XK_l, 	   focusstack, {.i = +1 } },
	{ MODKEY, 			  XK_h,        focusstack, {.i = -1 } },
	{ MODKEY, 			  XK_i,        incnmaster, {.i = +1 } },
	{ MODKEY, 			  XK_d,        incnmaster, {.i = -1 } },
	{ MODKEY|ShiftMask,   XK_h,        setmfact, {.f = -0.05} },
	{ MODKEY|ShiftMask,   XK_l,        setmfact, {.f = +0.05} },
	{ MODKEY, 			  XK_v,        zoom, {0} },
	{ MODKEY, 			  XK_Tab,      view, {0} },
	{ MODKEY|ShiftMask,   XK_c, 	   killclient, {0} },
	{ MODKEY|ShiftMask,   XK_space,    togglefloating, {0} },
	{ MODKEY, 			  XK_0,        view, {.ui = ~0 } },
	{ MODKEY|ShiftMask,   XK_0,        tag, {.ui = ~0 } },
	{ MODKEY, 			  XK_comma,    focusmon, {.i = -1 } },
	{ MODKEY, 			  XK_period,   focusmon, {.i = +1 } },
	{ MODKEY|ShiftMask,   XK_comma,    tagmon, {.i = -1 } },
	{ MODKEY|ShiftMask,   XK_period,   tagmon, {.i = +1 } },
	{ MODKEY|ControlMask, XK_comma,    cyclelayout,{.i = -1 } },
	{ MODKEY|ControlMask, XK_period,   cyclelayout,{.i = +1 } },
	{ MODKEY|ShiftMask,   XK_q,        quit, {0} },
	{ 0,                  XF86XK_AudioLowerVolume, spawn, {.v = downvol } },
	{ 0,                  XF86XK_AudioMute, spawn, {.v = mutevol } },
	{ 0,                  XF86XK_AudioRaiseVolume, spawn, {.v = upvol } },
	TAGKEYS(XK_1, 0)
	TAGKEYS(XK_2, 1)
	TAGKEYS(XK_3, 2)
	TAGKEYS(XK_4, 3)
	TAGKEYS(XK_5, 4)
	TAGKEYS(XK_6, 5)
	TAGKEYS(XK_7, 6)
	TAGKEYS(XK_8, 7)
	TAGKEYS(XK_9, 8)
};

/* button definitions */
static Button buttons[] = {
	{ ClkLtSymbol, 0, Button1, setlayout, {0} },
	{ ClkLtSymbol, 0, Button3, setlayout, {.v = &layouts[2]} },
	{ ClkWinTitle, 0, Button2, zoom, {0} },
	{ ClkStatusText, 0, Button2, spawn, {.v = termcmd } },
	{ ClkClientWin, MODKEY, Button1, movemouse, {0} },
	{ ClkClientWin, MODKEY, Button2, togglefloating, {0} },
	{ ClkClientWin, MODKEY, Button3, resizemouse, {0} },
	{ ClkTagBar, 0, Button1, view, {0} },
	{ ClkTagBar, 0, Button3, toggleview, {0} },
	{ ClkTagBar, MODKEY, Button1, tag, {0} },
	{ ClkTagBar, MODKEY, Button3, toggletag, {0} },
};
