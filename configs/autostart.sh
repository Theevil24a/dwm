setroot -cursor_name left_ptr &
nvidia-settings --assign CurrentMetaMode="nvidia-auto-select +0+0 { ForceFullCompositionPipeline = On }" &
xsettingsd &
nm-applet &
xss-lock --transfer-sleep-lock -- i3lock --nofork &
dex --autostart --environment dwm &
if [[ ! `pidof xfce-polkit` ]]; then /usr/lib/xfce-polkit/xfce-polkit & fi
#dwmblocks 2>/dev/null &
slstatus 2>/dev/null &
xfce4-power-manager &
dunst -geom "280x50-10+42" -frame_width "1" -font "Iosevka Custom 12"  -lb "$BACKGROUND" -lf "$FOREGROUND" -lfr "$BLUE" -nb "$BACKGROUND" -nf "$FOREGROUND" -nfr "$BLUE" -cb "$BACKGROUND" -cf "$RED" -cfr "$RED" &
wmname LG3D &
picom --config .config/picom.conf &
export _JAVA_AWT_WM_NONREPARENTING=1 &
bash ~/screenlayout/start.sh
nitrogen --restore
lxsession
